import json
from functools import wraps


def get_json(view_func):
    @wraps(view_func)
    def wrapper_view_func(request, *args, **kwargs):
        request.json = None
        if request.content_type == 'application/json':
            try:
                request.json = json.loads(request.body)
            except:
                pass
        return view_func(request, *args, **kwargs)
    return wrapper_view_func
