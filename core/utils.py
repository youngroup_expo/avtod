import base64
import hashlib
import json

import logging

import time
import statistics
import requests


class Avtocod:

    token = None
    url = "https://b2bapi.avtocod.ru/b2b/api/v1/"
    report_type = 'Expobank_extended_test_report'
    proxies = {
        'http': 'http://22.176.35.131:8080',
        'https': 'http://22.176.35.131:8080'
    }

    def __init__(self, _username, _password, _stamp, _age):
        """
        :param _username:
        :param _password:
        :param _stamp: token lifetime start time in long time type
        :param _age: the lifetime of the token in seconds
        """
        self.token = self.token_generate(_username, _password, _stamp, _age)

    def token_generate(self, username, password, stamp, age):
        password_hash = base64.b64encode(bytes.fromhex(hashlib.md5(password.encode()).hexdigest())).decode("utf-8")
        salted_pass = "%s:%s:%s"%(str(stamp), str(age), password_hash)
        salted_hash = base64.b64encode(bytes.fromhex(hashlib.md5(salted_pass.encode()).hexdigest())).decode("utf-8")
        token = "%s:%s:%s:%s"%(username, str(stamp), str(age), salted_hash)
        token_hash = base64.b64encode(bytes(token.encode())).decode("utf-8")
        return token_hash

    def report_request(self, query_type, query_value, report_type=report_type):
        url = self.url+"user/reports/%s/_make"%report_type
        headers = {"Content-Type":"application/json", "Authorization": self.token}
        payload = {"queryType": query_type, "query": query_value}
        response = json.loads(requests.post(url, data=json.dumps(payload), headers=headers, proxies=self.proxies).text)
        logging.warning(str(response))
        is_success = response["state"] != "fail"
        result = response["data"][0]["uid"] if is_success else response["event"]["message"]
        return is_success, result

    def report_parse(self, report):
        content = report['data'][0]['content']
        price = statistics.mean(map(lambda x: x['amount'], content['car_price']['items']))
        year = content['tech_data']['year']
        engine_size = content['tech_data']['engine']['volume']
        color = content['tech_data']['body']['color']['name']
        reg_num = content['identifiers']['vehicle']['reg_num']
        vin = content['identifiers']['vehicle']['vin']

    def report_get(self, uid):
        url = self.url+"user/reports/%s?_detailed=true&_content=true"%uid
        headers = {"Content-Type": "application/json",
                   "Authorization": "AR-REST %s" % self.token}
        response = json.loads(requests.get(url, headers=headers, proxies=self.proxies).text)
        old_time = time.time()
        while int(response['data'][0]['progress_wait'])!=0:
            delta = time.time()-old_time
            sleep_time = 5 if delta<=60 else 30 if delta>60 and delta<=301 else 10*60
            time.sleep(sleep_time)
            response = json.loads(requests.get(url, headers=headers, proxies=self.proxies).text)
        return response

    def get_report_status(self, uid):
        url = self.url + "user/reports/%s?_detailed=true&_content=true" % uid
        headers = {"Content-Type": "application/json",
                   "Authorization": "AR-REST %s" % self.token}
        response = json.loads(requests.get(url, headers=headers, proxies=self.proxies).text)
        status = 100 - response['data']['progress_wait']*100/(response['data']['progress_wait']+response['data']['progress_error'])
        return 'Complete %d%'%status


def dict_value_or_none(dict, *argv):
    """Try to get the value specified by the dictionary path.

    Example:
    dict_value_or_none(dict, 'some', 'path', 'here')
    tries to get value from dict['some']['path']['here'].
    If path doesn't exist in dict then dict_value_or_none
    returns None"""
    try:
        value = dict
        for arg in argv:
            value = value[arg]
        return value
    except:
        return None


def has_not_none(dict):
    """Return True if dict has at least one not None value
    otherwise return False"""
    for k, v in dict.items():
        if v is not None:
            return True
    return False


def int_or_none(val):
    try:
        return int(val)
    except:
        return None


def float_or_none(val):
    try:
        return float(val)
    except:
        return None
