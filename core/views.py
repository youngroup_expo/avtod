from django.contrib import messages
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from .decorators import *
from .services import *
from .models import *
from .utils import dict_value_or_none
import logging
import json


@method_decorator(csrf_exempt, name='dispatch')
class ApiAddCars(View):

    @method_decorator(get_json)
    def post(self, request):
        if request.json is None:
            logging.warning('Expected json, but got: ContentType - ' +
                            request.content_type + ', Body - ' + str(request.body))
            return JsonResponse({'Success': 'False', 'Reason': 'Json expected'})
        save_result = save_json_type1(request.json)
        if save_result:
            return JsonResponse({'Success': 'True'})
        else:
            return JsonResponse({'Success': 'False'})


@method_decorator(csrf_exempt, name='dispatch')
class ApiUpdateCars(View):

    @method_decorator(get_json)
    def post(self, request):
        if request.json is None:
            logging.warning('Expected json, but got: ContentType - ' +
                            request.content_type + ', Body - ' + str(request.body))
            return JsonResponse({'Success': 'False', 'Reason': 'Json expected'})
        update_result = update_cars_json_type2(request.json)
        if update_result:
            return JsonResponse({'Success': 'True'})
        else:
            return JsonResponse({'Success': 'False'})


def index(request):
    cars = Car.objects.all()
    return render(request, 'core/index.html', {'cars': cars})


def report(request, car_id):
    car = get_object_or_404(Car, pk=car_id)
    report = None
    if hasattr(car, 'avtocodreport'):
        report = car.avtocodreport

    images_data = {}
    images = car.images.all()
    for image in images:
        if image.url == '':
            url = image.image.url
        else:
            url = image.url

        if image.name in images_data:
            images_data[image.name].append(url)
        else:
            images_data[image.name] = [url]

    return render(request,
                  'core/report.html',
                  {
                      'vin': car.vin,
                      'report': report,
                      'images': images_data
                  })

def upload(request):
    if request.method == 'POST':
        if 'json_file' not in request.FILES:
            messages.error(request, 'Вы не выбрали файл.')
            return render(request, 'core/upload.html')

        file = request.FILES['json_file']
        data = file.read()
        json_data = {}
        try:
            json_data = json.loads(data.decode('utf-8-sig'))
        except:
            try:
                json_data = json.loads(data.decode('utf-8'))
            except:
                try:
                    json_data = json.loads(data.decode('cp1251'))
                except Exception as e:
                    raise e

        zalog = dict_value_or_none(json_data, 'Cars', 'Zalog')
        if zalog is None:
            result = save_json_type1(json_data)
        else:
            result = update_cars_json_type2(json_data)

        if isinstance(result, tuple):
            messages.error(request, 'Машина с VIN ' + result[1] + ' была загружена ранее')
            return render(request, 'core/upload.html')
        elif result:
            messages.success(request, 'Файл успешно обработан')
            return redirect('index')
        else:
            messages.error(request, 'Произошла ошибка при ' +
                'обработке файла')
            return render(request, 'core/upload.html')

    # GET and others
    return render(request, 'core/upload.html')


