from django.contrib.postgres.fields import JSONField
from django.db import models


class JsonType1(models.Model):
    """I don't know the name of the service sendind requests
    to this app. So I call this class in a common way"""
    json = JSONField()
    created_at = models.DateTimeField(auto_now_add=True)


class CompanyGroup(models.Model):
    name = models.CharField(max_length=100)
    id = models.PositiveIntegerField(primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    source_json = models.ForeignKey(JsonType1, on_delete=models.CASCADE)


class Company(models.Model):
    inn = models.BigIntegerField()
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    group = models.ForeignKey(CompanyGroup, on_delete=models.CASCADE)
    source_json = models.ForeignKey(JsonType1, on_delete=models.CASCADE)


class Car(models.Model):
    new = models.BooleanField()
    for_loan = models.BooleanField()
    make = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17)
    engine_size = models.FloatField()
    color = models.CharField(max_length=100)
    car_number = models.CharField(max_length=15)
    mileage = models.BigIntegerField()
    pts = models.CharField(max_length=15)
    address = models.CharField(max_length=500)
    price = models.BigIntegerField()
    zalog = models.PositiveSmallIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    company = models.ForeignKey(Company, null=True, on_delete=models.CASCADE)
    company_group = models.ForeignKey(CompanyGroup, null=True, on_delete=models.CASCADE)
    source_json = models.ForeignKey(JsonType1, on_delete=models.CASCADE)


class CarImage(models.Model):
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=500, blank=True)
    image = models.ImageField(upload_to='car_image', blank=True)
    car = models.ForeignKey(Car, related_name='images', on_delete=models.CASCADE)        


class AvtocodReport(models.Model):
    ready = models.BooleanField(default=False)
    data = JSONField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    car = models.OneToOneField(Car, on_delete=models.CASCADE)
