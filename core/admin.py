from django.contrib import admin

from .models import *

admin.site.register(Car)
admin.site.register(CompanyGroup)
admin.site.register(Company)
admin.site.register(AvtocodReport)
admin.site.register(JsonType1)
admin.site.register(CarImage)
