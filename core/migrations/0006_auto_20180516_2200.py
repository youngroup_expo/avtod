# Generated by Django 2.0.5 on 2018-05-16 22:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20180515_1822'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='carimage',
            name='base64',
        ),
        migrations.AddField(
            model_name='carimage',
            name='image',
            field=models.ImageField(blank=True, upload_to='car_image'),
        ),
        migrations.AlterField(
            model_name='car',
            name='company',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Company'),
        ),
        migrations.AlterField(
            model_name='car',
            name='company_group',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='core.CompanyGroup'),
        ),
        migrations.AlterField(
            model_name='carimage',
            name='url',
            field=models.CharField(blank=True, default='', max_length=500),
            preserve_default=False,
        ),
    ]
