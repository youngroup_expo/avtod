from __future__ import absolute_import, unicode_literals
from celery import shared_task
from .models import *
from .utils import *

import time
import logging
import traceback
import base64
import requests
import os


@shared_task
def check_avtocod(report_id):
    try:
        report = AvtocodReport.objects.get(pk=report_id)
        avtocod = Avtocod("grinko@Expobank", "cROud9em",
                          int(time.time()) - 60, 60 * 60 * 24)
        success, uid = avtocod.report_request('VIN', report.car.vin)
        report.data = avtocod.report_get(uid)
        report.ready = True
        report.save()
    except Exception as e:
        logging.error(traceback.format_exc())
        logging.error('Something has gone wrong: ' + str(e))


