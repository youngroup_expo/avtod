from django import template
import json

register = template.Library()

@register.filter(name='tojson')
def tojson(val):
    return json.dumps(val, indent=4, ensure_ascii=False)