from django.core.files.base import ContentFile
from .models import *
from .utils import *
from .tasks import *
import base64
import logging
import traceback
import requests
import os


def reduce_json_data(json_data):
    """Because of base64 images the log file becomes too big.
    So we should reduce the json data before writing it to the log"""

    if isinstance(json_data, dict):
        new = {}
        for k, v in json_data.items():
            if k == 'Images':
                v = '***reduced***'

            else:
                v = reduce_json_data(v)

            new[k] = v
        return new
    
    if isinstance(json_data, list):
        new_list = []
        for el in json_data:
            new_list.append(reduce_json_data(el))
        return new_list

    return json_data


def rstr(data):
    return str(reduce_json_data(data))


def save_json_type1(json_data):
    try:
        json_type_1 = JsonType1(json=reduce_json_data(json_data))
        json_type_1.save()
        companies_data = dict_value_or_none(json_data, 'Cars', 'Companies')
        if companies_data is None:
            logging.warning('Company groups info is empty. Json data: ' +
                            rstr(json_data))

        elif not save_company_group(companies_data, json_type_1):
            return False

        cars_data = dict_value_or_none(json_data, 'Cars', 'Car')
        if cars_data is None:
            logging.warning('Cars info is empty. Json data: ' +
                            rstr(json_data))
            return False

        return save_cars(cars_data, json_type_1)

    except Exception as e:
        logging.error(traceback.format_exc())
        logging.error('Something has gone wrong: ' + str(e))
        return False


def save_company_group(companies_data, source_json):
    # cg means company group
    for cg_data in companies_data:
        cg_fields = {
            'id': dict_value_or_none(cg_data, 'CompanyGroupId'),
            'name': dict_value_or_none(cg_data, 'CompanyGroupName')
        }
        if not has_not_none(cg_fields):
            logging.warning('Company group fields are empty. ' +
                            'Companies\' data from json: ' + str(companies_data))
            return False

        if cg_fields['id'] is None:
            logging.warning('Company group\'s data doesn\'t contain ' +
                            'id value. Companies\' data from json: ' +
                            str(companies_data))
            return False

        cg_fields['id'] = int(cg_fields['id'])
        existing_cg = CompanyGroup.objects.filter(id=cg_fields['id'])
        if existing_cg:
            cg = existing_cg.first()
            if cg.name != cg_fields['name']:
                logging.warning('The Company group\'s existing ' +
                                'name (' + cg.name + ') is diffenrent from ' +
                                'json_data\'s one (' + cg_fields['name'] + ').' +
                                'Companies\' data from json: ' + str(companies_data))
        else:
            cg = CompanyGroup(**cg_fields)
            cg.source_json = source_json
            cg.save()

        if 'GroupCompanies' not in cg_data:
            logging.warning('Company group\'s data doesn\'t contain ' +
                            'companies info. Companies\' data from json: ' +
                            str(companies_data))
            return False

        if not save_group_companies(cg_data['GroupCompanies'], cg, source_json):
            return False

    return True


def save_group_companies(group_companies_data, company_group, source_json):
    # c means company
    for c_data in group_companies_data:
        c_fields = {
            'inn': dict_value_or_none(c_data, 'ComapnyINN'),
            'name': dict_value_or_none(c_data, 'CompanyName')
        }
        if not has_not_none(c_fields):
            logging.warning('Company fields are empty fields. ' +
                            'Group companies\' data from json: ' +
                            str(group_companies_data))
            return False

        c_fields['inn'] = int(c_fields['inn'])
        logging.warning(str(c_fields))
        existing_c = Company.objects.filter(inn=c_fields['inn'])
        if existing_c:
            c = existing_c.first()
            if c.name != c_fields['name']:
                logging.warning('The Group company\'s existing ' +
                                'name (' + c.name + ') is diffenrent from ' +
                                'json_data\'s one (' + c_fields['name'] + ').' +
                                'Group companies\' data from json: ' +
                                str(group_companies_data))

            if c.group != company_group:
                logging.warning('The Group of existing company ' +
                                '(id: ' + str(c.group.id) + ') is ' +
                                'diffenrent from new one (id: ' +
                                str(company_group.id) + ').')
        else:
            c = Company(**c_fields)
            c.group = company_group
            c.source_json = source_json
            c.save()

    return True


def save_cars(cars, source_json):
    for car_data in cars:
        car_fields = {
            'make': dict_value_or_none(car_data, 'Make'),
            'model': dict_value_or_none(car_data, 'Model'),
            'year': dict_value_or_none(car_data, 'Year'),
            'vin': dict_value_or_none(car_data, 'VIN'),
            'engine_size': dict_value_or_none(car_data, 'EngineSize'),
            'color': dict_value_or_none(car_data, 'Color'),
            'car_number': dict_value_or_none(car_data, 'Gosnomer'),
            'mileage': dict_value_or_none(car_data, 'Kilometrage'),
            'pts': dict_value_or_none(car_data, 'PTS'),
            'price': dict_value_or_none(car_data, 'Price'),
            'city': dict_value_or_none(car_data, 'City'),
            'street': dict_value_or_none(car_data, 'Street')
        }
        # At first check if dictionary has at least one not None value
        if not has_not_none(car_fields):
            logging.warning('Car fields are empty. ' +
                            'Cars\' data from json: ' + rstr(cars))
            return False
        # Then modify dictionary to satisfy car model
        car_fields['address'] = (str(car_fields['city'] or '') + ', ' +
                                 str(car_fields['street'] or ''))
        del car_fields['city']
        del car_fields['street']
        car_fields['year'] = int_or_none(car_fields['year'])
        car_fields['engine_size'] = float_or_none(car_fields['engine_size'])
        car_fields['mileage'] = int_or_none(car_fields['mileage'])
        car_fields['price'] = int_or_none(car_fields['price'])

        vin = car_fields['vin']
        if vin is None:
            logging.warning('VIN value must be set in car data. ' +
                            'Cars\' data from json: ' + rstr(cars))
            return False

        if Car.objects.filter(vin=vin).exists():
            logging.warning('The car with VIN = ' + vin + ' already exists. ' +
                            'Cars\' data from json: ' + rstr(cars))
            return False, vin

        company_group = None
        company_group_id = dict_value_or_none(car_data, 'CompanyGroupId')
        if company_group_id is None:
            logging.warning('Car\'s company group id is empty.')
        else:
            existing_cg = CompanyGroup.objects.filter(id=company_group_id)
            if not existing_cg:
                logging.warning('Can\'t find company group with id = ' +
                                company_group_id + '.')
            else:
                company_group = existing_cg.first()

        company = None
        company_inn = dict_value_or_none(car_data, 'CompanyINN')
        if company_inn is None:
            logging.warning('Car\'s company inn is empty.')
        else:
            existing_c = Company.objects.filter(inn=company_inn)
            if not existing_c:
                logging.warning('Can\'t find company with inn = ' +
                                company_inn + '.')
            else:
                company = existing_c.first()

        for_loan = dict_value_or_none(car_data, 'ForLoan')
        if for_loan is None:
            logging.warning('ForLoan value must be set in car data. ' +
                            'Cars\' data from json: ' + rstr(cars))
            return False

        car = Car(**car_fields)
        car.company = company
        car.company_group = company_group
        car.source_json = source_json
        if car.mileage < 1000:
            car.new = True
        else:
            car.new = False
        if for_loan == '1':
            car.zalog = 1
            car.for_loan = True
            car.save()

            avtocod_report = AvtocodReport(data={}, car=car)
            avtocod_report.save()

            check_avtocod.delay(avtocod_report.id)
        else:
            car.zalog = 0
            car.for_loan = False
            car.save()

        images = dict_value_or_none(car_data, 'Images')
        if images is not None:

            for images_group, image_array in images.items():
                for image_data in image_array:
                    if '-url' not in image_data:
                        continue

                    image = CarImage(name=images_group,
                                     car=car)
                    image.save()

                    if len(image_data['-url']) > 500:
                        image_data = ContentFile(
                            base64.b64decode(image_data['-url']))
                        image_name = ('car_' + str(car.id) +
                                      '_img_' + str(image.id) + '.jpg')
                        image.image.save(image_name, image_data, save=True)
                    else:
                        image.url = image_data['-url']
                    image.save()

    return True


def update_cars_json_type2(json_data):
    try:
        zalog_data = dict_value_or_none(json_data, 'Cars', 'Zalog')
        if zalog_data is None:
            logging.warning('Zalog info is empty. Json data: ' +
                            str(json_data))
            return False

        for car_data in zalog_data:
            vin = dict_value_or_none(car_data, 'VIN')
            if vin is None:
                logging.warning('VIN value must be set in car data. ' +
                                'Zalog data from json: ' + str(zalog_data))
                return False

            new_zalog_value = dict_value_or_none(car_data, 'Zalog')
            if new_zalog_value is None:
                logging.warning('Zalog value must be set in car data. ' +
                                'Zalog data from json: ' + str(zalog_data))
                return False

            changed_rows = Car.objects.filter(
                vin=vin).update(zalog=new_zalog_value)
            if changed_rows == 0:
                logging.warning('There is no car with VIN = ' + vin +
                                '. Zalog data from json: ' + str(zalog_data))
                return False

        return True

    except Exception as e:
        logging.error(traceback.format_exc())
        logging.error('Something has gone wrong: ' + str(e))
        return False
