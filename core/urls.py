from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('report/<int:car_id>/', views.report, name='report'),
    path('upload', views.upload, name='upload'),
    path('api/add_cars', views.ApiAddCars.as_view(), name='api.add_cars'),
    path('api/update_cars', views.ApiUpdateCars.as_view(), name='api.update_cars'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)